Design patterns:
1. Creational: These patterns are designed for class instantiation. They can be either class-creation patterns or object-creational patterns.

2. Structural: These patterns are designed with regard to a class's structure and composition. The main goal of most of these patterns is to increase the functionality of the class(es) involved, without changing much of its composition.

3. Behavioral: These patterns are designed depending on how one class communicates with others.


SOLID:
https://www.digitalocean.com/community/conceptual_articles/s-o-l-i-d-the-first-five-principles-of-object-oriented-design


CLEAN code:
1. https://gist.github.com/wojteklu/73c6914cc446146b8b533c0988cf8d29
2. https://github.com/jbarroso/clean-code