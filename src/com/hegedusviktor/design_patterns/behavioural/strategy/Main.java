package com.hegedusviktor.design_patterns.behavioural.strategy;

/**
 *
 * In Strategy pattern, a class behavior or its algorithm
 * can be changed at run time. This type of design pattern comes under behavior pattern.
 *
 * In Strategy pattern, we create objects which represent
 * various strategies and a context object whose behavior varies
 * as per its strategy object. The strategy object changes the executing algorithm of the context object.
 *
 * for example: sorting algorithms depends on input
 * https://www.tutorialspoint.com/design_pattern/strategy_pattern.htm
 */


interface Strategy {
    int doOperation(int a, int b);
}

class AddOperation implements Strategy{
    @Override
    public int doOperation(int a, int b) {
        System.out.println(a+b);
        return a+b;
    }
}

class SubtractOperation implements Strategy{
    @Override
    public int doOperation(int a, int b) {
        System.out.println(a-b);
        return a-b;
    }
}

class Context {
    private Strategy strategy;

    public Context(Strategy strategy) {
        this.strategy = strategy;
    }

    public void setStrategy(Strategy strategy) {
        this.strategy = strategy;
    }

    public void executeStrategy(int a, int b) {
        if (this.strategy == null) return;
        this.strategy.doOperation(a, b);
    }

    public Strategy getStrategy() {
        return strategy;
    }
}

public class Main {
    public static void main(String[] args) {
        Context context = new Context(new AddOperation());
        context.executeStrategy(10, 10);

        context.setStrategy(new SubtractOperation());
        context.executeStrategy(12, 10);
    }
}
