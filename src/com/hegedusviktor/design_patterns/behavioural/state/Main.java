package com.hegedusviktor.design_patterns.behavioural.state;

/**
 * https://www.youtube.com/watch?v=MGEx35FjBuo
 * https://www.tutorialspoint.com/design_pattern/state_pattern.htm
 * https://www.geeksforgeeks.org/state-design-pattern/
 *
 * In State pattern a class behavior changes based on its state.
 * This type of design pattern comes under behavior pattern.
 *
 * In State pattern, we create objects which represent various
 * states and a context object whose behavior varies as its state object changes.
 */

interface State {
    void doAction(Context context);
}

class StartState implements State{
    public void doAction(Context context) {
     context.setState(this);
    }

    @Override
    public String toString() {
        return "Start state toString()";
    }
}

class StopState implements State{
    public void doAction(Context context) {
        context.setState(this);
    }

    @Override
    public String toString() {
        return "Stop state toString()";
    }
}

class Context {
    private State state;

    public Context() {
        setState(new StartState());
    }

    public void setState(State state) {
        this.state = state;
    }
    public State getState() {
        return state;
    }
}



public class Main {
    public static void main(String[] args) {
        Context context = new Context();
        System.out.println(context.getState());

        State stopState = new StopState();
        stopState.doAction(context);
        System.out.println(context.getState());
    }

}
