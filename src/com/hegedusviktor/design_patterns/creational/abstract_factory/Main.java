package com.hegedusviktor.design_patterns.creational.abstract_factory;

/**
 * abstract factory
 * create factories by their interfaces
 */


class Rectangle implements IShape {
    @Override
    public void draw() {
        System.out.println("Hello, Rectangle");
    }
}

class Square implements IShape {
    @Override
    public void draw() {
        System.out.println("Hello, Square");
    }
}

class Circle implements IShape {
    @Override
    public void draw() {
        System.out.println("Hello, Circle");
    }
}

//**********************************************

class RedColor implements IColor {
    @Override
    public void fill() {
        System.out.println("Hello, Redcolor");
    }
}

class BlueColor implements IColor {
    @Override
    public void fill() {
        System.out.println("Hello, Bluecolor");
    }
}

//**********************************************

class ShapeFactory extends AbstractFactory {
    public IShape getShape(String type) {
        if (type == null) return null;
        if (type.equalsIgnoreCase("SQUARE")) return new Square();
        if (type.equalsIgnoreCase("RECTANGLE")) return new Rectangle();
        if (type.equalsIgnoreCase("CIRCLE")) return new Circle();
        return null;
    }

    @Override
    public IColor getColor(String color) {
        return null;
    }
}


class ColorFactory extends AbstractFactory {
    @Override
    public IColor getColor(String color) {
        if (color == null) return null;
        if (color.equalsIgnoreCase("RED")) return new RedColor();
        if (color.equalsIgnoreCase("RECTANGLE")) return new BlueColor();
        return null;
    }

    @Override
    public IShape getShape(String type) {
        return null;
    }
}

//**********************************************

class FactoryProducer {
    public static AbstractFactory getFactory(String type) {
        if (type == null) return null;
        if (type.equalsIgnoreCase("SHAPE")) return new ShapeFactory();
        if (type.equalsIgnoreCase("COLOR")) return new ColorFactory();
        return null;
    }
}

//**********************************************


public class Main {

    public static void main(String[] args) {
        AbstractFactory abstractFactory1 = FactoryProducer.getFactory("SHAPE");
        AbstractFactory abstractFactory2 = FactoryProducer.getFactory("COLOR");
        IShape shape = abstractFactory1.getShape("CIRCLE");
        IColor color = abstractFactory2.getColor("RED");
        shape.draw();
        color.fill();
    }
}
