package com.hegedusviktor.design_patterns.creational.abstract_factory;

public interface IColor {
    void fill();
}
