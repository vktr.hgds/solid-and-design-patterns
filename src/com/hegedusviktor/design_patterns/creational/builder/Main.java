package com.hegedusviktor.design_patterns.creational.builder;

 /**
 * The builder pattern is a good choice when designing classes whose constructors or static factories would have more than a handful of parameters.
 * Pizza(int size) { ... }
 * Pizza(int size, boolean cheese) { ... }
 * Pizza(int size, boolean cheese, boolean pepperoni) { ... }
 * Pizza(int size, boolean cheese, boolean pepperoni, boolean bacon) { ... }
 *
 * One alternative you have to the
 * Telescoping Constructor Pattern is the JavaBean Pattern where you call a constructor with the mandatory parameters and then call any optional setters after:
 *
 * Pizza pizza = new Pizza(12);
 * pizza.setCheese(true);
 * pizza.setPepperoni(true);
 * pizza.setBacon(true);
 *
 * Setter VS Builder
 * It allows the builder to check that the fields are consistent (e.g. if foo is set to something, then bar must be set as well), which is messier to do with setters.
 * More importantly, it allows all the fields to be final, and that gives some important advantages in multi-threaded environments.
 *
 * By using setters, you can not make ( or difficult to make) class immutable. which can be achieved by using builder pattern.
 *
 * source:https://howtodoinjava.com/design-patterns/creational/builder-pattern-in-java/
 *
 */

class User {
    //All final attributes
    private final String firstName; // required
    private final String lastName; // required
    private final int age; // optional
    private final String phone; // optional
    private final String address; // optional

    private User(UserBuilder builder) {
        this.firstName = builder.firstName;
        this.lastName = builder.lastName;
        this.age = builder.age;
        this.phone = builder.phone;
        this.address = builder.address;
    }

    //All getter, and NO setter to provide immutability
    public String getFirstName() {
        return firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public int getAge() {
        return age;
    }
    public String getPhone() {
        return phone;
    }
    public String getAddress() {
        return address;
    }

    @Override
    public String toString() {
        return "User: "+this.firstName+", "+this.lastName+", "+this.age+", "+this.phone+", "+this.address;
    }

    public static class UserBuilder
    {
        private final String firstName;
        private final String lastName;
        private int age;
        private String phone;
        private String address;

        public UserBuilder(String firstName, String lastName) {
            this.firstName = firstName;
            this.lastName = lastName;
        }
        public UserBuilder age(int age) {
            this.age = age;
            return this;
        }
        public UserBuilder phone(String phone) {
            this.phone = phone;
            return this;
        }
        public UserBuilder address(String address) {
            this.address = address;
            return this;
        }
        //Return the finally constructed User object
        public User build () {
            User user =  new User(this);
            validateUserObject(user);
            return user;
        }
        private void validateUserObject(User user) {
            //Do some basic validations to check
            //if user object does not break any assumption of system
        }
    }
}

public class Main {
    public static void main(String[] args) {
        User user = new User.UserBuilder("FirstName", "LastName")
                .address("proba")
                .age(10)
                .phone("10")
                .build();
        System.out.println(user.toString());

        User user2 = new User.UserBuilder("FirstName", "LastName")
                .address("proba")
                .build();
        System.out.println(user2.toString());
    }
}
