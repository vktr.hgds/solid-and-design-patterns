package com.hegedusviktor.design_patterns.creational.singleton;

/**
 * A singleton should be used when managing access to a resource which is shared by the entire application
 * Reading configuration files that should only be read at startup time and encapsulating them in a Singleton.
 *
 * - Private Constructor
 * - Static Private Instance Variable of Singleton class itself
 * - Public getter method only at very first time it will initialize
 * - above variable, and always return same instance of a Singleton class.
 *
 * The Singleton Design Pattern aims to keep a check on initialization
 * of objects of a particular class by ensuring that only one instance of the object exists throughout the Java Virtual Machine.
 *
 */

public class Main {
    public static void main(String[] args) {
        Singleton singleton = Singleton.getInstance();
        System.out.println(singleton.toString());
    }
}
