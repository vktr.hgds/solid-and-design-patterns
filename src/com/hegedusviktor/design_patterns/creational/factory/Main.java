package com.hegedusviktor.design_patterns.creational.factory;

/**
 * The Factory Design Pattern or Factory Method Design Pattern is one of the most used design patterns in Java.
 *
 * According to GoF, this pattern “defines an interface for creating an object,
 * but let subclasses decide which class to instantiate. The Factory method lets a class defer instantiation to subclasses”.
 */

class Rectangle implements IShape {
    @Override
    public void draw() {
        System.out.println("Hello, Rectangle");
    }
}

class Square implements IShape {
    @Override
    public void draw() {
        System.out.println("Hello, Square");
    }
}

class Circle implements IShape {
    @Override
    public void draw() {
        System.out.println("Hello, Circle");
    }
}


class ShapeFactory {
    public IShape getShape(String type) {
        if (type == null) return null;
        if (type.equalsIgnoreCase("SQUARE")) return new Square();
        if (type.equalsIgnoreCase("RECTANGLE")) return new Rectangle();
        if (type.equalsIgnoreCase("CIRCLE")) return new Circle();
        return null;
    }

}


public class Main {
    public static void main(String[] args) {
        ShapeFactory shapeFactory = new ShapeFactory();
        IShape shape = shapeFactory.getShape("CIRCLE");
        IShape shape1 = shapeFactory.getShape("SQUARE");
        shape.draw();
        shape1.draw();
    }

}
