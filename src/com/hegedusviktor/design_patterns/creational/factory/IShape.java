package com.hegedusviktor.design_patterns.creational.factory;

public interface IShape {
    void draw();
}
