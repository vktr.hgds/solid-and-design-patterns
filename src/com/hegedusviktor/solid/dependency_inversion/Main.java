package com.hegedusviktor.solid.dependency_inversion;

/**
 * loosely coupled code is the main priority
 * dependeny should be in the constructor, then we wont be depend on only one implementation
 */

interface IBoard {
    void board();
}

class ThreeByThreeBoard implements IBoard {
    private String[] spaces;

    public void board() {
        this.spaces = new String[]{"0", "1", "2", "3", "4", "5", "6", "7", "8"};
    }

    public String[] getSpaces() {
        return spaces;
    }

    public void setSpaces(String[] spaces) {
        this.spaces = spaces;
    }
}


class FourByFourBoard implements IBoard {
    private String[] spaces;

    public void board() {
        this.spaces = new String[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"};
    }

    public String[] getSpaces() {
        return spaces;
    }

    public void setSpaces(String[] spaces) {
        this.spaces = spaces;
    }
}

class Game {
    private IBoard iBoard;

    public Game(IBoard iBoard) {
        this.iBoard = iBoard;
    }

    public IBoard getiBoard() {
        return iBoard;
    }

    public void setiBoard(IBoard iBoard) {
        this.iBoard = iBoard;
    }
}


public class Main {
    public static void main(String[] args) {
        Game game = new Game(new FourByFourBoard());
        Game game1 = new Game(new ThreeByThreeBoard());
    }
}
