package com.hegedusviktor.solid.liskov_substitution;

public class Bird {
    public void eat() {
        System.out.println("Im eating...");
    }
}
