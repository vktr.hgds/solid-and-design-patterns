package com.hegedusviktor.solid.liskov_substitution;

public class FlyingBird extends Bird {
    public void fly() {
        System.out.println("Im flying...");
    }
}
