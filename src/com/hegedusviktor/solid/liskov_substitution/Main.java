package com.hegedusviktor.solid.liskov_substitution;

/**
 * Liskov Substitution Principle (LSP) applies to inheritance hierarchies,
 * specifying that you should design your classes so that client dependencies can be substituted
 * with subclasses without the client knowing about the change.
 * All subclasses must, therefore, operate in the same manner as their base classes.
 */

/**
 * This principle is just an extension of the Open Close Principle and it means that we
 * must make sure that new derived classes are extending the base classes without changing their behavior.
 */

public class Main {
    public static void main(String[] args) {
        FlyingBird bird = new Duck();
        bird.fly();
        bird.eat();

        Bird bird1 = new Ostrich();
        bird1.eat();
    }

}
