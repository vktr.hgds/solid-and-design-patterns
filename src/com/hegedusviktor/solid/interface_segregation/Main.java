package com.hegedusviktor.solid.interface_segregation;

/**
 * Difference Between Injection vs Inversion
 * Dependency Injection is an Inversion of Control technique for supplying objects
 * ('dependencies') to a class by way of the Dependency Injection Design Pattern.
 * Typically passing dependencies via one of the following:
 *
 * A constructor
 * A public property or field
 * A public setter
 * The Dependency Inversion Principle (DIP) is a software design guideline which boils
 * down to two recommendations about de-coupling a class from its concrete dependencies:
 *
 * 'High-level modules should not depend on low-level modules. Both should depend on abstractions.'
 * 'Abstractions should not depend upon details. Details should depend upon abstractions.'
 * Or, to put it even more succinctly:
 *
 * Dependency Injection is an implementation technique for populating instance variables of a class.
 * Dependency Inversion is a general design guideline which recommends that classes should
 * only have direct relationships with high-level abstractions.
 */


interface Length {
    int getLength();
}
interface Width {
    int getWidth();
}
interface Radius {
    int getRadius();
}
interface Area {
    double getArea();
}

class Rectangle implements Length, Width, Area {
    int length;
    int width;
    public Rectangle(int length, int width) {
        this.length = length;
        this.width = width;
    }
    public int getLength() {
        return length;
    }
    public int getWidth() {
        return width;
    }
    public int getRadius() {
        // implementation
        return 0;
    }
    public double getArea() {
        return width * length;
    }
}
class Square implements Length, Area {
    int length;

    public Square(int length) {
        this.length = length;
    }
    public int getLength() {
        return length;
    }
    public int getWidth() {
        // implementation
        return 0;
    }
    public int getRadius() {
        // implementation
        return 0;
    }
    public double getArea() {
        return length * length;
    }
}

class Circle implements Radius, Area {
    int radius;
    public Circle(int radius) {
        this.radius = radius;
    }
    public int getLength() {
        // implementation
        return 0;
    }
    public int getWidth() {
        // implementation
        return 0;
    }
    public int getRadius() {
        return radius;
    }
    public double getArea() {
        return 3.14* radius * radius;
    }
}

public class Main {
    public static void main(String[] args) {
        Rectangle r = new Rectangle(10,20);
        Square s = new Square(15);
        Circle c = new Circle(2);

        System.out.println("Rectangle area:"+r.getArea());
        System.out.println("Square area:"+s.getArea());
        System.out.println("Circle area:"+c.getArea());
    }
}
