package com.hegedusviktor.solid.open_closed;

public interface ICalculator {
    void calculate(IOperation operation);
}
