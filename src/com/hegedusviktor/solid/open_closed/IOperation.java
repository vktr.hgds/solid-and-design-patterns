package com.hegedusviktor.solid.open_closed;

public interface IOperation {
    int perform();
}
