package com.hegedusviktor.solid.open_closed;

public class Calculator implements ICalculator {
    @Override
    public void calculate(IOperation ioperation) {
        if (ioperation == null) return;
        System.out.println(ioperation.perform());
    }

}
