package com.hegedusviktor.solid.open_closed;

/**
 * https://howtodoinjava.com/design-patterns/open-closed-principle/
 */

public class Main {
    public static void main(String[] args) {
        IOperation addition = new Addition(10, 2);
        IOperation subtraction = new Subtraction(10, 2);

        Calculator calc = new Calculator();
        calc.calculate(addition);
        calc.calculate(subtraction);
    }
}
